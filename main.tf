terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.72.0"
}

provider "yandex" {
  service_account_key_file = "key.json" 
  cloud_id  = var.ya_cloud_id
  folder_id = var.ya_folder_id
  zone      = var.zone
}


# Configure the AWS Provider
provider "aws" {
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
  region = "eu-west-1"
}

data "aws_route53_zone" "main" {
  name = "devops.rebrain.srwx.net"
}

data "yandex_vpc_subnet" "default" {
  name = "default-${var.zone}"
}

data "yandex_compute_image" "base_image" {
  family = var.yc_image_family
}

resource "random_string" "password" {
  count = length(var.devs)
  length = "16"
  upper = true
  lower = true
  special = false
  number = true
}
## Create a new Yandex Cloud instance
resource "yandex_compute_instance" "node" {
  count       = length(var.devs)
  name        = "linux-vm-${count.index}"
  hostname    = "linux-vm-${count.index}"
  platform_id = var.platform_id
  labels = var.label
  
  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      image_id = data.yandex_compute_image.base_image.id
      size = var.disk_size
    }
  }

  network_interface {
    subnet_id = data.yandex_vpc_subnet.default.id
    nat       = true
  }

  metadata = {
    ssh-keys = "ubuntu:${file(var.pub_key)}"
  }


  connection {
    host = self.network_interface.0.nat_ip_address
    type = "ssh"
    user = "ubuntu"
    agent = false
    private_key = file(var.private_key)

  }
  
  provisioner "remote-exec" {
    inline = [
      "yes ${element(random_string.password.*.result, count.index)} | sudo passwd ubuntu",
      "sudo sleep 30",
      "sudo sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/' /etc/ssh/sshd_config",
      "sudo sed -i 's/#PubkeyAuthentication yes/PubkeyAuthentication no/' /etc/ssh/sshd_config",
      "sudo service ssh restart",
    ]
  }

   provisioner "local-exec" {
     
     command = "echo ${var.user}-${count.index}.devops.rebrain.srwx.net ubuntu ${element(random_string.password.*.result, count.index)}"
   }


}

resource "aws_route53_record" "devops_dns" {
  count   = length(var.devs)
  allow_overwrite = true
  zone_id = data.aws_route53_zone.main.zone_id
  name    = "${element(var.devs, count.index)}"
  type    = "A"
  ttl     = "300"
  records = ["${element(yandex_compute_instance.node.*.network_interface.0.nat_ip_address, count.index)}"]
}

resource "local_file" "template" {
    count = length(var.devs)
    content     = templatefile("template.tftpl", { name = aws_route53_record.devops_dns.*.fqdn,
                                                   ip = yandex_compute_instance.node.*.network_interface.0.nat_ip_address,
                                                   pass = random_string.password.*.result
                                                 }
                              )
    filename = "${path.module}/output.txt"

}

