# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/aws" {
  version = "4.4.0"
  hashes = [
    "h1:xUXge6/Bn/CzSjZpmQIr7/FwANKj+3cIEnxYlgS1xFo=",
  ]
}

provider "registry.terraform.io/hashicorp/local" {
  version = "2.1.0"
  hashes = [
    "h1:EYZdckuGU3n6APs97nS2LxZm3dDtGqyM4qaIvsmac8o=",
  ]
}

provider "registry.terraform.io/hashicorp/random" {
  version = "3.1.0"
  hashes = [
    "h1:BZMEPucF+pbu9gsPk0G0BHx7YP04+tKdq2MrRDF1EDM=",
  ]
}

provider "registry.terraform.io/yandex-cloud/yandex" {
  version = "0.72.0"
  hashes = [
    "h1:qRApY45wZpnd3+3PfoRQClR90dcJRvdiOHfYvUV4do0=",
  ]
}
